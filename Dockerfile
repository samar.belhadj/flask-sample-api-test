FROM python:3


WORKDIR /app

COPY ./src/requirements.txt .

RUN pip install -r requirements.txt

COPY . .

EXPOSE 5000

ENTRYPOINT ["uwsgi", "--ini", "uwsgi.ini"]
