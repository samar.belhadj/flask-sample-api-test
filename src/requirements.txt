-i https://pypi.org/simple
aniso8601==9.0.1
blinker==1.6.2 ; python_version >= '3.7'
click==8.1.3 ; python_version >= '3.7'
flask==2.3.2 ; python_version >= '3.8'
flask-marshmallow==0.15.0
flask-restful==0.3.9
flask-sqlalchemy==3.0.3
greenlet==2.0.2 ; platform_machine == 'aarch64' or (platform_machine == 'ppc64le' or (platform_machine == 'x86_64' or (platform_machine == 'amd64' or (platform_machine == 'AMD64' or (platform_machine == 'win32' or platform_machine == 'WIN32')))))
importlib-metadata==6.6.0 ; python_version < '3.10'
itsdangerous==2.1.2 ; python_version >= '3.7'
jinja2==3.1.2 ; python_version >= '3.7'
markupsafe==2.1.2 ; python_version >= '3.7'
marshmallow==3.19.0 ; python_version >= '3.7'
marshmallow-sqlalchemy==0.29.0
packaging==23.1 ; python_version >= '3.7'
pytz==2023.3
six==1.16.0 ; python_version >= '2.7' and python_version not in '3.0, 3.1, 3.2, 3.3'
sqlalchemy==2.0.12 ; python_version >= '3.7'
typing-extensions==4.5.0 ; python_version >= '3.7'
uwsgi==2.0.21
werkzeug==2.3.3 ; python_version >= '3.8'
zipp==3.15.0 ; python_version >= '3.7'
